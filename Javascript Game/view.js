let i1 = true
let i2 = true
let i3 = true

function RefreshTable() {

    while (table.firstChild) {
        table.removeChild(table.firstChild);
    }

    for (let i = 0; i < 5; i++) {
        const tr = document.createElement("tr")
        for (let j = 0; j < 5; j++) {
            const td = document.createElement("td")

            if (matrix[i][j].IsPlayer) {
                const img = document.createElement("img")
                switch (actualPlayer) {
                    case 1:
                        img.src = "Assets/Player1.png"
                        break;

                    case 2:
                        img.src = "Assets/Player2.png"
                        break;

                    case 3:
                        img.src = "Assets/Player3.png"
                        break;

                    case 4:
                        img.src = "Assets/Player4.png"
                        break;
                }
                td.appendChild(img);
            }

            if (matrix[i][j].IsDug) {

                td.style.backgroundColor = "rgb(254, 215, 168)"
                const img = document.createElement("img")
                switch (matrix[i][j].Type) {

                    case "Item1":
                        if (IndexOfClue1_R() != null && IndexOfClue1_C() != null) {
                            img.src = "Assets/Item 1.png";
                        }
                        else {
                            img.src = "Assets/Hole.png"
                        }
                        break;

                    case "Item2":
                        if (IndexOfClue2_R() != null && IndexOfClue2_C() != null) {
                            img.src = "Assets/Item 2.png";
                        }
                        else {
                            img.src = "Assets/Hole.png"
                        }
                        break;

                    case "Item3":
                        if (IndexOfClue3_R() != null && IndexOfClue3_C() != null) {
                            img.src = "Assets/Item 3.png";
                        }
                        else {
                            img.src = "Assets/Hole.png"
                        }
                        break;

                    case "ItemClue1_U":
                        img.src = "Assets/Item 1 - clue_UP.png"
                        break;

                    case "ItemClue1_D":
                        img.src = "Assets/Item 1 - clue_DOWN.png"
                        break;

                    case "ItemClue1_L":
                        img.src = "Assets/Item 1 - clue_LEFT.png"
                        break;

                    case "ItemClue1_R":
                        img.src = "Assets/Item 1 - clue_RIGHT.png"
                        break;

                    case "ItemClue2_U":
                        img.src = "Assets/Item 2 - clue_UP.png"
                        break;

                    case "ItemClue2_D":
                        img.src = "Assets/Item 2 - clue_DOWN.png"
                        break;

                    case "ItemClue2_L":
                        img.src = "Assets/Item 2 - clue_LEFT.png"
                        break;

                    case "ItemClue2_R":
                        img.src = "Assets/Item 2 - clue_RIGHT.png"
                        break;

                    case "ItemClue3_U":
                        img.src = "Assets/Item 3 - clue_UP.png"
                        break;

                    case "ItemClue3_D":
                        img.src = "Assets/Item 3 - clue_DOWN.png"
                        break;

                    case "ItemClue3_L":
                        img.src = "Assets/Item 3 - clue_LEFT.png"
                        break;

                    case "ItemClue3_R":
                        img.src = "Assets/Item 3 - clue_RIGHT.png"
                        break;

                    case "StarGate":
                        img.src = "Assets/Stargate.png"
                        break;

                    case "Oasis":
                        img.src = "Assets/Oasis.png";
                        break;

                    case "Drought":
                        img.src = "Assets/Drought.png";
                        break;

                    case "Nothing":
                        img.src = "Assets/Hole.png";
                        break;

                    default:
                        break;

                }

                if (img.src != null) {
                    td.appendChild(img)
                }

                if (matrix[i][j].IsPlayer) {
                    td.style.border = "5px solid orange";
                } else {
                    td.style.border = "1px solid white;";
                }

            } else {

                const img = document.createElement("img")

                switch (matrix[i][j].Type) {

                    case "Oasis":
                        img.src = "Assets/Oasis marker.png"
                        break;

                    case "Drought":
                        img.src = "Assets/Oasis marker.png"
                        break;

                    case "StarGate":
                        img.src = "Assets/Stargate.png"
                        break;

                    default:
                        break;

                }

                if (img.src != null) {
                    td.appendChild(img)
                }

                if (matrix[i][j].IsPlayer) {
                    td.style.border = "5px solid orange";
                } else {
                    td.style.border = "1px solid white;";
                }
            }

            tr.appendChild(td)
        }
        table.appendChild(tr)
    }
    board.appendChild(table)


    while (detailsTable.firstChild) {
        detailsTable.removeChild(detailsTable.firstChild);
    }

    for (let j = 0; j < numberOfPlayers; j++) {
        const detailSection = document.createElement("section")

        switch (j) {
            case 0:
                detailSection.innerHTML = `${player1.Name}: <img src="Assets/Action Points.png"></img> ${player1.Action} <img src="Assets/Water.png"></img> ${player1.Water}`
                if (actualPlayer === 1) {
                    detailSection.style.color = "green"
                }
                else {
                    detailSection.style.color = "black"
                }
                break

            case 1:
                detailSection.innerHTML = `${player2.Name}: <img src="Assets/Action Points.png"></img> ${player2.Action} <img src="Assets/Water.png"></img> ${player2.Water}`
                if (actualPlayer === 2) {
                    detailSection.style.color = "red"
                }
                else {
                    detailSection.style.color = "black"
                }
                break

            case 2:
                detailSection.innerHTML = `${player3.Name}: <img src="Assets/Action Points.png"></img> ${player3.Action} <img src="Assets/Water.png"></img> ${player3.Water}`
                if (actualPlayer === 3) {
                    detailSection.style.color = "blue"
                }
                else {
                    detailSection.style.color = "black"
                }
                break

            case 3:
                detailSection.innerHTML = `${player4.Name}: <img src="Assets/Action Points.png"></img> ${player4.Action} <img src="Assets/Water.png"></img> ${player4.Water}`
                if (actualPlayer === 4) {
                    detailSection.style.color = "rgb(175, 12, 221)"
                }
                else {
                    detailSection.style.color = "black"
                }
                break
        }
        detailsTable.appendChild(detailSection)
    }

    details.appendChild(detailsTable)


    const items = document.createElement("section")
    items.style.alignSelf = "center"

    if (itemsFound.Item1 && i1) {
        const image = document.createElement("img")
        image.src = "Assets/Item 1.png"
        items.appendChild(image)
        i1 = false
        details.appendChild(items)
    }
    if (itemsFound.Item2 && i2) {
        const image = document.createElement("img")
        image.src = "Assets/Item 2.png"
        items.appendChild(image)
        i2 = false
        details.appendChild(items)
    }
    if (itemsFound.Item3 && i3) {
        const image = document.createElement("img")
        image.src = "Assets/Item 3.png"
        items.appendChild(image)
        i3 = false
        details.appendChild(items)
    }
}


function GameOverView(isWon) {
    if (isWon) {
        const span = document.createElement("span")
        span.style.height = "fit-content"
        span.style.alignSelf = "center"
        span.style.border = "2px solid black"
        span.style.margin = "5px 25px"
        span.style.backgroundColor = "orange"
        if (numberOfPlayers > 1) {
            span.innerText = "NYERTETEK!"
        }
        else {
            span.innerText = "NYERTÉL!"
        }

        details.appendChild(span)
    }
    else {
        const span = document.createElement("span")
        span.style.height = "fit-content"
        span.style.alignSelf = "center"
        span.style.border = "2px solid black"
        span.style.margin = "5px 25px"
        span.style.backgroundColor = "orange"
        if (numberOfPlayers > 1) {
            span.innerText = "VESZTETTETEK!"
        }
        else {
            span.innerText = "VESZTETTÉL!"
        }

        details.appendChild(span)
    }
}