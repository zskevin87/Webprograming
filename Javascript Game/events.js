let isPaused = false


function PlayerCoord() {
    for (i = 0; i < matrix.length; i++) {
        for (j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].IsPlayer) {
                return [i, j]
            }
        }
    }

    return -1;
}

function PlayerAction(e) {
    if (isPaused) {
        return;
    }

    const playerCoord = PlayerCoord();
    switch (e.key) {
        //Stepping
        case 'ArrowLeft':
            if (playerCoord[1] - 1 >= 0) {
                matrix[playerCoord[0]][playerCoord[1]].IsPlayer = false
                matrix[playerCoord[0]][playerCoord[1] - 1].IsPlayer = true

                switch (actualPlayer) {
                    case 1:
                        player1.Action--;

                        if (player1.Action === 0) {
                            player1.Water--

                            if (player1.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player1.Action = 3
                        }

                        break;

                    case 2:
                        player2.Action--;

                        if (player2.Action === 0) {
                            player2.Water--

                            if (player2.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player2.Action = 3
                        }

                        break;

                    case 3:
                        player3.Action--;

                        if (player3.Action === 0) {
                            player3.Water--

                            if (player3.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player3.Action = 3
                        }

                        break;

                    case 4:
                        player4.Action--;

                        if (player4.Action === 0) {
                            player4.Water--

                            if (player4.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player4.Action = 3
                        }

                        break;

                }

                RefreshTable();
            }
            break;

        case 'ArrowRight':
            if (playerCoord[1] + 1 < matrix.length) {
                matrix[playerCoord[0]][playerCoord[1]].IsPlayer = false
                matrix[playerCoord[0]][playerCoord[1] + 1].IsPlayer = true
                switch (actualPlayer) {
                    case 1:
                        player1.Action--;

                        if (player1.Action === 0) {
                            player1.Water--

                            if (player1.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player1.Action = 3
                        }

                        break;

                    case 2:
                        player2.Action--;

                        if (player2.Action === 0) {
                            player2.Water--

                            if (player2.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player2.Action = 3
                        }

                        break;

                    case 3:
                        player3.Action--;

                        if (player3.Action === 0) {
                            player3.Water--

                            if (player3.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player3.Action = 3
                        }

                        break;

                    case 4:
                        player4.Action--;

                        if (player4.Action === 0) {
                            player4.Water--

                            if (player4.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player4.Action = 3
                        }

                        break;

                }
                RefreshTable();
            }
            break;

        case 'ArrowUp':
            if (playerCoord[0] - 1 >= 0) {
                matrix[playerCoord[0]][playerCoord[1]].IsPlayer = false
                matrix[playerCoord[0] - 1][playerCoord[1]].IsPlayer = true
                switch (actualPlayer) {
                    case 1:
                        player1.Action--;

                        if (player1.Action === 0) {
                            player1.Water--

                            if (player1.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player1.Action = 3
                        }

                        break;

                    case 2:
                        player2.Action--;

                        if (player2.Action === 0) {
                            player2.Water--

                            if (player2.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player2.Action = 3
                        }

                        break;

                    case 3:
                        player3.Action--;

                        if (player3.Action === 0) {
                            player3.Water--

                            if (player3.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player3.Action = 3
                        }

                        break;

                    case 4:
                        player4.Action--;

                        if (player4.Action === 0) {
                            player4.Water--

                            if (player4.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player4.Action = 3
                        }

                        break;

                }
                RefreshTable();
            }
            break;

        case 'ArrowDown':
            if (playerCoord[0] + 1 < matrix.length) {
                matrix[playerCoord[0]][playerCoord[1]].IsPlayer = false
                matrix[playerCoord[0] + 1][playerCoord[1]].IsPlayer = true
                switch (actualPlayer) {
                    case 1:
                        player1.Action--;

                        if (player1.Action === 0) {
                            player1.Water--

                            if (player1.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player1.Action = 3
                        }

                        break;

                    case 2:
                        player2.Action--;

                        if (player2.Action === 0) {
                            player2.Water--

                            if (player2.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player2.Action = 3
                        }

                        break;

                    case 3:
                        player3.Action--;

                        if (player3.Action === 0) {
                            player3.Water--

                            if (player3.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player3.Action = 3
                        }

                        break;

                    case 4:
                        player4.Action--;

                        if (player4.Action === 0) {
                            player4.Water--

                            if (player4.Water === 0) {
                                GameOver(false);
                            }
                            NextPlayer(PlayerCoord());
                            player4.Action = 3
                        }

                        break;

                }
                RefreshTable();
            }
            break;


        // Digging
        case ' ':
            if (!(matrix[playerCoord[0]][playerCoord[1]].IsDug)) {

                if (matrix[playerCoord[0]][playerCoord[1]].Type != "StarGate") {
                    matrix[playerCoord[0]][playerCoord[1]].IsDug = true
                }

                switch (matrix[playerCoord[0]][playerCoord[1]].Type) {
                    case "StarGate":
                        if (StarGateOpen) {
                            GameOver(true);
                        }
                        break;
                    case "ItemClue1_D":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound1_R = true
                        break;

                    case "ItemClue1_U":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound1_R = true
                        break;

                    case "ItemClue1_R":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound1_C = true
                        break;

                    case "ItemClue1_L":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound1_C = true
                        break;

                    case "ItemClue2_D":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound2_R = true
                        break;

                    case "ItemClue2_U":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound2_R = true
                        break;

                    case "ItemClue2_R":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound2_C = true
                        break;

                    case "ItemClue2_L":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound2_C = true
                        break;

                    case "ItemClue3_D":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound3_R = true
                        break;

                    case "ItemClue3_U":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound3_R = true
                        break;

                    case "ItemClue3_R":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound3_C = true
                        break;

                    case "ItemClue3_L":
                        matrix[playerCoord[0]][playerCoord[1]].CluesFound3_C = true
                        break;

                    default:
                        break;
                }

                if (!(itemsFound.foundItems.some(n => n === 1)) && matrix[playerCoord[0]][playerCoord[1]].Type == "Item1" && IndexOfClue1_R() != null && IndexOfClue1_C() != null) {
                    itemsFound.foundItems.push(1)
                    itemsFound.Item1 = true
                }
                else if (!(itemsFound.Item1) && IndexOfClue1_R() != null && IndexOfClue1_C() != null) {
                    matrix[IndexOfItem1()[0]][IndexOfItem1()[1]].IsDug = false
                }


                if (!(itemsFound.foundItems.some(n => n === 2)) && matrix[playerCoord[0]][playerCoord[1]].Type == "Item2" && IndexOfClue2_R() != null && IndexOfClue2_C() != null) {
                    itemsFound.foundItems.push(2)
                    itemsFound.Item2 = true
                }
                else if (!(itemsFound.Item2) && IndexOfClue2_R() != null && IndexOfClue2_C() != null) {
                    matrix[IndexOfItem2()[0]][IndexOfItem2()[1]].IsDug = false
                }


                if (!(itemsFound.foundItems.some(n => n === 3)) && matrix[playerCoord[0]][playerCoord[1]].Type == "Item3" && IndexOfClue3_R() != null && IndexOfClue3_C() != null) {
                    itemsFound.foundItems.push(3)
                    itemsFound.Item3 = true
                }
                else if (!(itemsFound.Item3) && IndexOfClue3_R() != null && IndexOfClue3_C() != null) {
                    matrix[IndexOfItem3()[0]][IndexOfItem3()[1]].IsDug = false
                }


                if (itemsFound.Item1 && itemsFound.Item2 && itemsFound.Item3) {
                    StarGateOpen = true
                }

                if (matrix[playerCoord[0]][playerCoord[1]].Type === "Oasis") {
                    switch (actualPlayer) {
                        case 1:
                            player1.Action--;
    
                            if (player1.Action === 0) {
                                NextPlayer(PlayerCoord());
                                player1.Action = 3
                            }
                            player1.Water = 6;
    
                            break;
    
                        case 2:
                            player2.Action--;
    
                            if (player2.Action === 0) {
                                NextPlayer(PlayerCoord());
                                player2.Action = 3
                            }
                            player2.Water = 6;
                            break;
    
                        case 3:
                            player3.Action--;
    
                            if (player3.Action === 0) {
                                NextPlayer(PlayerCoord());
                                player3.Action = 3
                            }
                            player3.Water = 6;
                            break;
    
                        case 4:
                            player4.Action--;
    
                            if (player4.Action === 0) {
                                NextPlayer(PlayerCoord());
                                player4.Action = 3
                            }
                            player4.Water = 6;
                            break;
    
                    }

                    RefreshTable();
                }
                else if (matrix[playerCoord[0]][playerCoord[1]].Type != "StarGate") {
                    switch (actualPlayer) {
                        case 1:
                            player1.Action--;
    
                            if (player1.Action === 0) {
                                player1.Water--
    
                                if (player1.Water === 0) {
                                    GameOver(false);
                                }
                                NextPlayer(PlayerCoord());
                                player1.Action = 3
                            }
    
                            break;
    
                        case 2:
                            player2.Action--;
    
                            if (player2.Action === 0) {
                                player2.Water--
    
                                if (player2.Water === 0) {
                                    GameOver(false);
                                }
                                NextPlayer(PlayerCoord());
                                player2.Action = 3
                            }
    
                            break;
    
                        case 3:
                            player3.Action--;
    
                            if (player3.Action === 0) {
                                player3.Water--
    
                                if (player3.Water === 0) {
                                    GameOver(false);
                                }
                                NextPlayer(PlayerCoord());
                                player3.Action = 3
                            }
    
                            break;
    
                        case 4:
                            player4.Action--;
    
                            if (player4.Action === 0) {
                                player4.Water--
    
                                if (player4.Water === 0) {
                                    GameOver(false);
                                }
                                NextPlayer(PlayerCoord());
                                player4.Action = 3
                            }
    
                            break;
    
                    }
                    RefreshTable();
                }
            }

            else if ((player1.Water < 6 || player2.Water < 6 || player3.Water < 6 || player4.Water < 6) && matrix[playerCoord[0]][playerCoord[1]].Type === "Oasis") {
                switch (actualPlayer) {
                    case 1:
                        player1.Action--;

                        if (player1.Action === 0) {
                            NextPlayer(PlayerCoord());
                            player1.Action = 3
                        }
                        player1.Water = 6;
                        break;

                    case 2:
                        player2.Action--;

                        if (player2.Action === 0) {
                            NextPlayer(PlayerCoord());
                            player2.Action = 3
                        }
                        player2.Water = 6;
                        break;

                    case 3:
                        player3.Action--;

                        if (player3.Action === 0) {
                            NextPlayer(PlayerCoord());
                            player3.Action = 3
                        }
                        player3.Water = 6;
                        break;

                    case 4:
                        player4.Action--;

                        if (player4.Action === 0) {
                            NextPlayer(PlayerCoord());
                            player4.Action = 3
                        }
                        player4.Water = 6;
                        break;

                }
            }
            RefreshTable();
    }
}

function GameOver(isWon) {
    GameOverView(isWon)
    isPaused = true
    StopTimer()

    const button = document.createElement("button")
    button.innerText = "Új játék"
    button.addEventListener("click", NewGame)

    button.style.height = "fit-content"
    button.style.alignSelf = "center"

    details.appendChild(button)
}


document.addEventListener("keydown", PlayerAction)