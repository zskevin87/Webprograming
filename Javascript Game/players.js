let actualPlayer;
let itemsFound;
let player1;
let player2;
let player3;
let player4;
let StarGateOpen = false

function DefaultPlayers() {
    actualPlayer = 1

    itemsFound = {
        Item1: false,
        Item2: false,
        Item3: false,
        foundItems: []
    }


    player1 = {
        IsActive: false,
        Name: "Játékos1",
        Action: 3,
        Water: 6,
        Position: [2, 2]
    }

    player2 = {
        IsActive: false,
        Name: "Játékos2",
        Action: 3,
        Water: 6,
        Position: [2, 2]
    }

    player3 = {
        IsActive: false,
        Name: "Játékos3",
        Action: 3,
        Water: 6,
        Position: [2, 2]
    }

    player4 = {
        IsActive: false,
        Name: "Játékos4",
        Action: 3,
        Water: 6,
        Position: [2, 2]
    }

    StarGateOpen = false
}

function InitializePlayers(activePlayers) {
    if (activePlayers === 4) {
        player1.IsActive = true
        player2.IsActive = true
        player3.IsActive = true
        player4.IsActive = true

        if (text1.value != "") {
            player1.Name = text1.value
        } else {
            player1.Name = "Játékos1"
        }

        if (text2.value != "") {
            player2.Name = text2.value
        } else {
            player2.Name = "Játékos2"
        }

        if (text3.value != "") {
            player3.Name = text3.value
        } else {
            player3.Name = "Játékos3"
        }

        if (text4.value != "") {
            player4.Name = text4.value
        } else {
            player4.Name = "Játékos4"
        }
    }

    else if (activePlayers === 3) {
        player1.IsActive = true
        player2.IsActive = true
        player3.IsActive = true
        player4.IsActive = false

        if (text1.value != "") {
            player1.Name = text1.value
        } else {
            player1.Name = "Játékos1"
        }

        if (text2.value != "") {
            player2.Name = text2.value
        } else {
            player2.Name = "Játékos2"
        }

        if (text3.value != "") {
            player3.Name = text3.value
        } else {
            player3.Name = "Játékos3"
        }
    }

    else if (activePlayers === 2) {
        player1.IsActive = true
        player2.IsActive = true
        player3.IsActive = false
        player4.IsActive = false

        if (text1.value != "") {
            player1.Name = text1.value
        } else {
            player1.Name = "Játékos1"
        }

        if (text2.value != "") {
            player2.Name = text2.value
        } else {
            player2.Name = "Játékos2"
        }
    }

    else {
        player1.IsActive = true
        player2.IsActive = false
        player3.IsActive = false
        player4.IsActive = false

        if (text1.value != "") {
            player1.Name = text1.value
        } else {
            player1.Name = "Játékos1"
        }
    }

    player1.Water = waterLevel.value
    player2.Water = waterLevel.value
    player3.Water = waterLevel.value
    player4.Water = waterLevel.value
}

function NextPlayer(coord) {
    if (player1.Action === 0 && numberOfPlayers > 1) {
        player1.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player2.Position[0]][player2.Position[1]].IsPlayer = true
        actualPlayer = 2
    }
    else if (player1.Action === 0 && numberOfPlayers === 1) {
        return
    }
    else if (player2.Action === 0 && numberOfPlayers > 2) {
        player2.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player3.Position[0]][player3.Position[1]].IsPlayer = true
        actualPlayer = 3
    }
    else if (player2.Action === 0 && numberOfPlayers === 2) {
        player2.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player1.Position[0]][player1.Position[1]].IsPlayer = true
        actualPlayer = 1
    }
    else if (player3.Action === 0 && numberOfPlayers > 3) {
        player3.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player4.Position[0]][player4.Position[1]].IsPlayer = true
        actualPlayer = 4
    }
    else if (player3.Action === 0 && numberOfPlayers === 3) {
        player3.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player1.Position[0]][player1.Position[1]].IsPlayer = true
        actualPlayer = 1
    }
    else if (player4.Action === 0 && numberOfPlayers === 4) {
        player4.Position = coord
        matrix[coord[0]][coord[1]].IsPlayer = false
        matrix[player1.Position[0]][player1.Position[1]].IsPlayer = true
        actualPlayer = 1
    }
}

DefaultPlayers()