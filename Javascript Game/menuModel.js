let numberOfPlayers = 0;
let radioButtonClicked = false

const body = document.querySelector("body")

const menuDiv = document.getElementById("menu")
const playerNumArticle = document.getElementById("playerNum")
const waterLevelArticle = document.getElementById("waterLevel")

const board = document.querySelector(".board")
const table = document.querySelector("#table")

const details = document.querySelector(".details")
const detailsTable = document.querySelector("#detailsTable")

details.style.display = "none"
board.style.display = "none"

const b1 = document.getElementById("b1")
const b2 = document.getElementById("b2")
const b3 = document.getElementById("b3")
const b4 = document.getElementById("b4")

const text1 = document.getElementById("text-1")
const text2 = document.getElementById("text-2")
const text3 = document.getElementById("text-3")
const text4 = document.getElementById("text-4")

text1.disabled = true
text2.disabled = true
text3.disabled = true
text4.disabled = true

const waterLevel = document.getElementById("water-level")
const liter = document.getElementById("liter")

const timePlayCheckBox = document.getElementById("timePlay")
const time = document.getElementById("time")
const timeLength = document.getElementById("timeLength")

const startButton = document.getElementById("startGame")

function SetPlayerNumber() {
    if (b4.checked) {
        numberOfPlayers = 4

        text1.disabled = false
        text2.disabled = false
        text3.disabled = false
        text4.disabled = false

        radioButtonClicked = true

    } else if (b3.checked) {
        numberOfPlayers = 3

        text1.disabled = false
        text2.disabled = false
        text3.disabled = false
        text4.disabled = true

        text4.value = null

        radioButtonClicked = true

    } else if (b2.checked) {
        numberOfPlayers = 2

        text1.disabled = false
        text2.disabled = false
        text3.disabled = true
        text4.disabled = true

        text3.value = null
        text4.value = null

        radioButtonClicked = true

    } else if (b1.checked) {
        numberOfPlayers = 1

        text1.disabled = false
        text2.disabled = true
        text3.disabled = true
        text4.disabled = true

        text2.value = null
        text3.value = null
        text4.value = null

        radioButtonClicked = true

    } else {
        text1.disabled = true
        text2.disabled = true
        text3.disabled = true
        text4.disabled = true

        text1.value = null
        text2.value = null
        text3.value = null
        text4.value = null

        radioButtonClicked = false
    }
}

function SetWaterLevel() {
    liter.innerText = `${waterLevel.value} L`
}

function SetTime() {
    timeLength.innerText = `${time.value} min`
}

function StartButtonClicked() {

    if (!radioButtonClicked) {
        
        if (document.getElementById("error")) {
            return
        }
        
        const errorSection = document.createElement("section")
        errorSection.id = "error"
        errorSection.style.backgroundColor = "red"
        errorSection.innerText = "Kérlek, jelöld be, hogy hányan játszanátok!"

        menuDiv.appendChild(errorSection)
        return
    }
    else if (document.getElementById("error")) {

        document.getElementById("error").style.display = "none"
    }

    if (b4.checked) {
        InitializePlayers(4)
    }
    else if (b3.checked) {
        InitializePlayers(3)
    }
    else if (b2.checked) {
        InitializePlayers(2)
    }
    else {
        InitializePlayers(1)
    }

    if (timePlayCheckBox.checked) {
        maxTime = parseInt(time.value)
    }

    InitializeGame()
    menuDiv.style.display = "none"
    details.style.display = "flex"
    board.style.display = "grid"
}

function NewGame() {

    location.reload();
}

playerNumArticle.addEventListener("click", SetPlayerNumber)
body.addEventListener("mousemove", SetWaterLevel)
timePlayCheckBox.addEventListener("click", function() {
    if(timePlayCheckBox.checked) {
        time.disabled = false
    }
    else {
        time.disabled = true
    }
})
body.addEventListener("mousemove", SetTime)
startButton.addEventListener("click", StartButtonClicked)