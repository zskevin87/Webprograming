let matrix = []

function CreateTable(playerNum) {

    for (let i = 0; i < 5; i++) {
        let matrixRow = []
        for (let j = 0; j < 5; j++) {

            let field;

            if (i === 2 && j === 2) {
                field = {
                    Type: "StarGate",
                    IsDug: false,
                    IsPlayer: true,
                    CluesFound1_R: false,
                    CluesFound1_C: false,
                    CluesFound2_R: false,
                    CluesFound2_C: false,
                    CluesFound3_R: false,
                    CluesFound3_C: false
                }
            } else {
                field = {
                    Type: "Nothing",
                    IsDug: false,
                    IsPlayer: false,
                    CluesFound1_R: false,
                    CluesFound1_C: false,
                    CluesFound2_R: false,
                    CluesFound2_C: false,
                    CluesFound3_R: false,
                    CluesFound3_C: false
                }
            }
            matrixRow.push(field)
        }
        matrix.push(matrixRow)
    }
}

function ClueSpawner() {
    let itemNum = 3

    while (itemNum != 0) {
        let rowNum = parseInt(5 * Math.random())
        let colNum = parseInt(5 * Math.random())

        if (matrix[rowNum][colNum].Type === "Nothing") {

            let clueSpawnedInRow = false
            let clueSpawnedInCol = false

            switch (itemNum) {

                case 3:
                    matrix[rowNum][colNum].Type = "Item1"

                    //

                    while (!(clueSpawnedInRow && clueSpawnedInCol)) {

                        if (!clueSpawnedInRow) {
                            let anotherRowNum = parseInt(5 * Math.random())

                            if (matrix[anotherRowNum][colNum].Type === "Nothing") {
                                if (anotherRowNum < rowNum) {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue1_D"
                                }
                                else {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue1_U"
                                }

                                clueSpawnedInRow = true
                            }
                        }

                        if (!clueSpawnedInCol) {
                            let anotherColNum = parseInt(5 * Math.random())

                            if (matrix[rowNum][anotherColNum].Type === "Nothing") {
                                if (anotherColNum < colNum) {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue1_R"
                                }
                                else {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue1_L"
                                }

                                clueSpawnedInCol = true
                            }
                        }
                    }
                    itemNum--;

                    break;

                case 2:
                    matrix[rowNum][colNum].Type = "Item2"

                    while (!(clueSpawnedInRow && clueSpawnedInCol)) {

                        if (!clueSpawnedInRow) {
                            let anotherRowNum = parseInt(5 * Math.random())

                            if (matrix[anotherRowNum][colNum].Type === "Nothing") {
                                if (anotherRowNum < rowNum) {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue2_D"
                                }
                                else {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue2_U"
                                }

                                clueSpawnedInRow = true
                            }
                        }

                        if (!clueSpawnedInCol) {
                            let anotherColNum = parseInt(5 * Math.random())

                            if (matrix[rowNum][anotherColNum].Type === "Nothing") {
                                if (anotherColNum < colNum) {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue2_R"
                                }
                                else {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue2_L"
                                }

                                clueSpawnedInCol = true
                            }
                        }
                    }
                    itemNum--;

                    break;

                case 1:
                    matrix[rowNum][colNum].Type = "Item3"

                    while (!(clueSpawnedInRow && clueSpawnedInCol)) {

                        if (!clueSpawnedInRow) {
                            let anotherRowNum = parseInt(5 * Math.random())

                            if (matrix[anotherRowNum][colNum].Type === "Nothing") {
                                if (anotherRowNum < rowNum) {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue3_D"
                                }
                                else {
                                    matrix[anotherRowNum][colNum].Type = "ItemClue3_U"
                                }

                                clueSpawnedInRow = true
                            }
                        }

                        if (!clueSpawnedInCol) {
                            let anotherColNum = parseInt(5 * Math.random())

                            if (matrix[rowNum][anotherColNum].Type === "Nothing") {
                                if (anotherColNum < colNum) {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue3_R"
                                }
                                else {
                                    matrix[rowNum][anotherColNum].Type = "ItemClue3_L"
                                }

                                clueSpawnedInCol = true
                            }
                        }
                    }
                    itemNum--;

                    break;

                default:
                    break;
            }
        }
    }
}

function OasisSpawner() {
    let oasisNum = 4;

    while (oasisNum != 0) {
        let rowNum = parseInt(5 * Math.random())
        let colNum = parseInt(5 * Math.random())

        if (matrix[rowNum][colNum].Type === "Nothing") {
            if (oasisNum != 4) {
                matrix[rowNum][colNum].Type = "Oasis"
            }
            else {
                matrix[rowNum][colNum].Type = "Drought"
            }
            oasisNum--
        }
    }
}


//QUERIES

function IndexOfItem1() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].Type == "Item1") {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfItem2() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].Type == "Item2") {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfItem3() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].Type == "Item3") {
                return [i, j]
            }
        }
    }
    return null;
}



function IndexOfClue1_R() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound1_R) {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfClue1_C() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound1_C) {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfClue2_R() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound2_R) {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfClue2_C() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound2_C) {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfClue3_R() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound3_R) {
                return [i, j]
            }
        }
    }
    return null;
}

function IndexOfClue3_C() {
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j].CluesFound3_C) {
                return [i, j]
            }
        }
    }
    return null;
}

function InitializeGame() {
    CreateTable();
    ClueSpawner();
    OasisSpawner();
    RefreshTable();
    StartTimer();
}

