const timerElement = document.getElementById('timer');

let elapsedTimeInSeconds = 0;
let timerInterval;

let maxTime;


function StartTimer() {
    timerInterval = setInterval(() => {
    elapsedTimeInSeconds++;
    UpdateTimerDisplay();
}, 1000);
}

function UpdateTimerDisplay() {
    const minutes = Math.floor(elapsedTimeInSeconds / 60);
    const seconds = elapsedTimeInSeconds % 60;
    timerElement.innerText = `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    if (timePlayCheckBox.checked && minutes >= maxTime) {
        GameOver(false);
    }
}

function StopTimer() {
    clearInterval(timerInterval);
}