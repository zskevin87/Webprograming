# Javascript Game Assignment

In Webprograming course, I had to assign two larger code:
- a Javascript game
- a PHP bookstore webpage

The Javascript one can be found in this project.

Non of each program has been coded in any frameworks (like React, etc.).

## The exact task (in Hungarian):
[Javascript Task](https://gitlab.com/zskevin87/Webprograming/-/blob/main/task.pdf)